<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="org.deniszm.tamagotchi.service.PetsList" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tamagotchi: Select Pet</title>
</head>
<body>
    <%PetsList pets = (PetsList) session.getAttribute("pets");%>

    <form name="test" method="post" action="Play">

        <p><b>Выбирите питомца:</b></p>
            <c:forEach var="i" begin="1" end="${pets.getSize()}">

                <input type="radio" name="petId" value=${i}>
                 ${pets.get(i - 1).getName()} (энергия: ${pets.get(i - 1).getEnergy()}) <Br>

            </c:forEach>

        <p><input type="submit" value="Отправить"></p>
    </form>
</body>
</html>
