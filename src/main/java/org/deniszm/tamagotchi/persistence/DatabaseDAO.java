package org.deniszm.tamagotchi.persistence;

import org.deniszm.tamagotchi.model.Pet;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by denis on 13.01.2017.
 */
public class DatabaseDAO implements PetDAO {

    private Connection connection;

    public DatabaseDAO(String dbURL, String user, String pwd)
            throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        this.connection = DriverManager.getConnection(dbURL, user, pwd);
    }

    @Override
    public List<Pet> load() {
        List<Pet> pets = new ArrayList();
        Pet pet;

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM save_game");

            while (resultSet.next()) {
                pet = PetBuilder.getPet(resultSet.getString("type"),
                        resultSet.getString("name"),
                        resultSet.getInt("energy"));
                pets.add(pet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pets;
    }

    @Override
    public void save(List<Pet> pets) {

        try (Statement statement = connection.createStatement();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "INSERT INTO save_game VALUES (?,?,?)"
             )) {
            statement.execute("DELETE FROM save_game");

            for (Pet pet : pets) {
                preparedStatement.setString(1, pet.getClass().getName());
                preparedStatement.setString(2, pet.getName());
                preparedStatement.setInt(3, pet.getEnergy());

                preparedStatement.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
