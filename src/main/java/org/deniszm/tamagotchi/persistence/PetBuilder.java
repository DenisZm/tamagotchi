package org.deniszm.tamagotchi.persistence;

import org.deniszm.tamagotchi.model.Pet;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

class PetBuilder {

    public static Pet getPet(String type, String name, int energy) {
        Class petClass;
        Pet pet = null;

        try {
            petClass = Class.forName(type);
            Object[] petProperty = new Object[]{name, energy};
            Constructor[] petConstructor = petClass.getDeclaredConstructors();
            pet = (Pet) petConstructor[0].newInstance(petProperty);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return pet;
    }
}
