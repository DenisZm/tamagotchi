package org.deniszm.tamagotchi.service;

import org.deniszm.tamagotchi.model.*;
import org.deniszm.tamagotchi.persistence.PetDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * List of available pets.
 * Created by denis on 01.12.16.
 */
public class PetsList {
    private List<Pet> pets = new ArrayList<>();
    private PetDAO dao;

    public PetsList(PetDAO dao) {
        this.dao = dao;
    }

    public void add(Pet pet) {
        this.pets.add(pet);
    }

    public Pet get(int index) {
        return this.pets.get(index);
    }

    public void delete(Pet pet) {
        this.pets.remove(pet);
    }

    public int getSize() {
        return pets.size();
    }

    public void save() {
        dao.save(pets);
    }

    public void load() {
        this.pets = dao.load();
    }

    public void init() {
        pets.clear();
        pets.add(new Cat("Котик", 100));
        pets.add(new Dog("Песик", 100));
        pets.add(new Hedgehog("Ежик", 100));
        pets.add(new Rabbit("Кролик", 100));
    }

}


