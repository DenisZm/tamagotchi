package org.deniszm.tamagotchi.model;

public class Dog extends Pet {
    private PlayDescription[] plays = new PlayDescription[]{
            new PlayDescription("ловит мячик", 20, 30),
            new PlayDescription("гоняется за своим хвостом", 10, 30),
            new PlayDescription("приносит тапочки", 5, 10),
            new PlayDescription("танцует на задних лапах", 5, 10),
            new PlayDescription("трется мордой", 5, 10)};

    public Dog(String name, int energy) {
        super(name, energy);
    }

    public String play() {
        int selector = (int) (Math.random() * plays.length);
        PlayDescription play = plays[selector];
        decreaseEnergy(play.getEnergyMin(), play.getEnergyMax());
        return play.getPlayTitle();
    }
}
