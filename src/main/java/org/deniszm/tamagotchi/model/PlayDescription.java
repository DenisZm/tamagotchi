package org.deniszm.tamagotchi.model;

public class PlayDescription {
    private String playTitle;
    private int energyMin;
    private int energyMax;

    public PlayDescription(String playTitle, int energyMin, int energyMax) {
        this.playTitle = playTitle;
        this.energyMin = energyMin;
        this.energyMax = energyMax;
    }

    public String getPlayTitle() {
        return playTitle;
    }

    public int getEnergyMin() {
        return energyMin;
    }

    public int getEnergyMax() {
        return energyMax;
    }
}
