package org.deniszm.tamagotchi.model;

public class Hedgehog extends Pet {
    private PlayDescription[] plays = new PlayDescription[]{
            new PlayDescription("бегает с яблоком на спине", 20, 30),
            new PlayDescription("ходит в тумане", 20, 30),
            new PlayDescription("смешно сопит носом", 5, 10)};

    public Hedgehog(String name, int energy) {
        super(name, energy);
    }

    public String play() {
        int selector = (int) (Math.random() * plays.length);
        PlayDescription play = plays[selector];
        decreaseEnergy(play.getEnergyMin(), play.getEnergyMax());
        return play.getPlayTitle();
    }
}
