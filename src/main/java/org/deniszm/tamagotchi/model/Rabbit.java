package org.deniszm.tamagotchi.model;

public class Rabbit extends Pet {
    private PlayDescription[] plays = new PlayDescription[]{
            new PlayDescription("грызет провод", 20, 30),
            new PlayDescription("вылезает из шляпы", 10, 20),
            new PlayDescription("прыгает в нору", 5, 10),
            new PlayDescription("ищет морковку", 20, 30)};

    public Rabbit(String name, int energy) {
        super(name, energy);
    }

    @Override
    public String play() {
        int selector = (int) (Math.random() * plays.length);
        PlayDescription play = plays[selector];
        decreaseEnergy(play.getEnergyMin(), play.getEnergyMax());
        return play.getPlayTitle();
    }
}
