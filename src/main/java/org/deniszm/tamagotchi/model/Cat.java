package org.deniszm.tamagotchi.model;

public class Cat extends Pet {
    private PlayDescription[] plays = new PlayDescription[]{
            new PlayDescription("гоняется за мышкой", 20, 30),
            new PlayDescription("висит на шторе", 10, 30),
            new PlayDescription("прыгает со шкафа на комод", 10, 30),
            new PlayDescription("топчется лапами (мур-мур...)", 5, 10)};

    public Cat(String name, int energy) {
        super(name, energy);
    }

    public String play() {
        int selector = (int) (Math.random() * plays.length);
        PlayDescription play = plays[selector];
        decreaseEnergy(play.getEnergyMin(), play.getEnergyMax());
        return play.getPlayTitle();
    }
}
