package org.deniszm.tamagotchi.servlet;

import org.deniszm.tamagotchi.service.PetsList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SelectPet", urlPatterns = {"/SelectPet"})
public class SelectPet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        PetsList pets = (PetsList) getServletContext().getAttribute("pets");

        if (action.equals("init")) {
            pets.init();
            System.out.println("Start new game");
        }

        if (action.equals("load")) {
            pets.load();
            System.out.println("Load saved game");
        }

        RequestDispatcher dispatcher = req.getRequestDispatcher("/select.jsp");
        dispatcher.forward(req, resp);
    }
}
