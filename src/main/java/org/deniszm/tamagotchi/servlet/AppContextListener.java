package org.deniszm.tamagotchi.servlet;

import org.deniszm.tamagotchi.service.PetsList;
import org.deniszm.tamagotchi.persistence.PetDAO;
import org.deniszm.tamagotchi.persistence.DatabaseDAO;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.SQLException;

@WebListener
public class AppContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext ctx = servletContextEvent.getServletContext();

        //initialize DB Connection
        String dbURL = ctx.getInitParameter("dbURL");
        String user = ctx.getInitParameter("dbUser");
        String pwd = ctx.getInitParameter("dbPassword");

        try {
            PetDAO dao = new DatabaseDAO(dbURL, user, pwd);
            PetsList pets = new PetsList(dao);
            ctx.setAttribute("pets", pets);
            System.out.println("Pets list initialized successfully.");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("Listener destroyed.");
    }
}
